package net.librebora.cli.command

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.types.file
import net.librebora.cli.model.DataSets
import net.librebora.support.csv.Parser
import org.joda.time.DateTime
import java.io.File

class CreateIndexCommand : CliktCommand(
    help = "Loads data models from CSV into the database.",
    name = "loadData"
) {

    companion object {
        private const val BUFFER_SIZE: Long = 1024 * 1024 * 150
    }

    private val file: File by option("-f", "--file").file(exists = true).required()
    private val datasetName: String by option("-d", "--dataset").required()

    private val loaders: Map<DataSets, () -> Unit> = mapOf(
        DataSets.PROFILES to this::loadProfiles
    )

    override fun run() {
        val dataSet: DataSets = DataSets.valueOf(datasetName.toUpperCase())

        if (loaders.containsKey(dataSet)) {
            loaders.getValue(dataSet)()
        } else {
            echo("Data set not supported $datasetName")
        }
    }

    private fun loadProfiles() {
        var header = true

        echo("${DateTime.now()} parsing csv and inserting new records")

        Parser(
            bufferSize = BUFFER_SIZE
        ).parse(
            csvFile = file.absolutePath
        ) { position, row ->
            header = false
        }

        echo("${DateTime.now()} parsing complete, saving data")
    }
}
