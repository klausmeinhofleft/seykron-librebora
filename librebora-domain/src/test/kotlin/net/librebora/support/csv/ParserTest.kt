package net.librebora.support.csv

import org.junit.Test
import java.io.File

class ParserTest {

    companion object {
        private const val BUFFER_SIZE: Long = 1024 * 1024 * 150
    }

    object AuthoritiesTable : Table() {
        val ministerio = string(0, "ministerio")
        val dependencia = string(1, "dependencia")
        val reparticion = string(2, "reparticion")
        val sigla = string(3, "sigla")
        val codigoReparticion = string(4, "codigo_reparticion")
        val apellido = string(5, "apellido")
        val nombre = string(6, "nombre")
        val cuil = string(7, "cuil")
        val email = string(8, "email")
        val nivel = string(9, "nivel")
    }

    class Authority : Entity() {
        companion object : EntityOperations(AuthoritiesTable)

        val ministerio by AuthoritiesTable.ministerio
        val dependencia by AuthoritiesTable.dependencia
        val reparticion by AuthoritiesTable.reparticion
        val sigla by AuthoritiesTable.sigla
        val codigoReparticion by AuthoritiesTable.codigoReparticion
        val apellido by AuthoritiesTable.apellido
        val nombre by AuthoritiesTable.nombre
        val cuil by AuthoritiesTable.cuil
        val email by AuthoritiesTable.email
        val nivel by AuthoritiesTable.nivel
   }

    private val source: String = "src/test/resources/autoridades-superiores-y-funcionarios.csv"

    @Test
    fun parse() {
        val index = Index(mapOf(
            AuthoritiesTable.cuil.name to AuthoritiesTable.cuil.index
        ))

        Parser(BUFFER_SIZE).parse(
            csvFile = source
        ) { position, row ->
            index.put(position, row)
        }

        File("target/csv-index-test").mkdirs()
        index.writeTo("target/csv-index-test")

        session(source, index) {
            val persons: List<Authority> = Authority.find(
                AuthoritiesTable.cuil to "20-21354808-6"
            )
            assert(persons.size == 1)
            assert(persons[0].toString() == "ministerio: Jefatura de Gabinete de Ministros; dependencia: DG Administración de Bases de Datos e Ingeniería de Proyectos; reparticion: GO Análisis de Datos; sigla: DGABDIP; codigo_reparticion: 68011610; apellido: SIMIONATI; nombre:  OSCAR EDUARDO; cuil: 20-21354808-6; email: ; nivel: Carrera Gerencial")
            println(persons[0])
        }
    }
}
