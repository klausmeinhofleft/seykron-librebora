package net.librebora.support.csv

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.RandomAccessFile
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel.MapMode

class Parser(private val bufferSize: Long = DEFAULT_BUFFER_SIZE) {

    companion object {
        // Comma
        private const val SEPARATOR: Byte = 44
        private const val DOUBLE_QUOTE: Byte = 34
        private const val LINE_FEED: Byte = 10
        private const val CARRIAGE_RETURN: Byte = 13
        private const val ESCAPE: Byte = 92
        private const val DEFAULT_BUFFER_SIZE: Long = 1024 * 1024 * 50

        fun parseRecord(rawRecord: ByteArray): List<Field> {
            var withinField = false
            var escape = false
            var startIndex = 0
            val record: MutableList<Field> = mutableListOf()
            var addend = 0

            for (index in 0 until rawRecord.size) {
                val char = rawRecord[index]

                when {
                    !escape && !withinField && char == SEPARATOR -> {
                        record.add(Field(
                            rawRecord.copyOfRange(startIndex + addend, startIndex + (index - startIndex) - addend)
                        ))
                        startIndex = index + 1
                        addend = 0
                    }
                    !escape && char == DOUBLE_QUOTE -> {
                        if (!withinField) {
                            addend += 1
                        }

                        withinField = !withinField
                    }
                    !escape && char == ESCAPE ->
                        escape = true
                    escape -> {
                        escape = false
                    }
                }
            }

            return record
        }
    }

    private val logger: Logger = LoggerFactory.getLogger(Parser::class.java)

    fun parse(
        csvFile: String,
        callback: (Position, List<Field>) -> Unit
    ) {

        logger.info("parsing csv started")

        val handle = RandomAccessFile(csvFile, "r")
        val lineBreakSize = System.lineSeparator().length
        var pointer: Long = 0
        var tail = ByteArray(0)

        logger.info("csv line reader ready for sending records")

        while(true) {
            val bytesRead = if (pointer + bufferSize > handle.length()) {
                handle.length() - pointer
            } else {
                bufferSize
            }
            logger.info("reading $bytesRead bytes")

            val buffer: MappedByteBuffer = handle.channel.map(
                MapMode.READ_ONLY, pointer, bytesRead
            )
            var offset = 0
            var lineStart = 0

            while (offset < bytesRead) {
                val char = buffer[offset]

                // Linux & MAC line breaks.
                // TODO: support Windows
                if (char == LINE_FEED || char == CARRIAGE_RETURN) {

                    val lineEnd = lineStart + (offset - lineStart)
                    val line = if (tail.isNotEmpty()) {
                        val lineWithTail = tail + readLine(buffer, lineStart, lineEnd)
                        tail = ByteArray(0)
                        lineWithTail
                    } else
                        readLine(buffer, lineStart, lineEnd)

                    callback(Position(
                        start = pointer + lineStart,
                        end = pointer + lineEnd
                    ), parseRecord(line))

                    buffer.position(buffer.position() + lineBreakSize)
                    offset += lineBreakSize
                    lineStart = offset
                } else {
                    offset += 1
                }
            }

            if (bytesRead < bufferSize) {
                break
            }

            tail = readLine(buffer, lineStart, bytesRead.toInt())
            pointer += bytesRead
        }

        logger.info("parsing csv finished")
    }

    private fun readLine(
        buffer: MappedByteBuffer,
        start: Int,
        end: Int
    ): ByteArray {
        val lineBuffer = ByteArray(end - start)
        buffer.get(lineBuffer)
        return lineBuffer
    }
}
