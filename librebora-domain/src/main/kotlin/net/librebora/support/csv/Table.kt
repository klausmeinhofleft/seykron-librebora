package net.librebora.support.csv

import kotlin.reflect.KProperty

abstract class Table {

    data class StringColumn(
        override val index: Int,
        override val name: String
    ) : Column {
        operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
            require(thisRef is Entity)
            return thisRef.records[this]?.value ?: ""
        }
    }

    data class IntColumn(
        override val index: Int,
        override val name: String
    ) : Column {
        operator fun getValue(thisRef: Any?, property: KProperty<*>): Int {
            require(thisRef is Entity)
            return thisRef.records[this]?.value?.toInt() ?: 0
        }
    }

    var columns: List<Column> = listOf()

    fun string(
        index: Int,
        name: String
    ): StringColumn = addColumn(StringColumn(index = index, name = name))

    fun int(
        index: Int,
        name: String
    ): IntColumn = addColumn(IntColumn(index = index, name = name))

    private fun<T : Column> addColumn(column: T): T {
        columns = columns + column
        return column
    }
}
