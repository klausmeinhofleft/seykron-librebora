package net.librebora.support.csv

open class Entity {
    lateinit var records: MutableMap<Column, Field>

    override fun toString(): String {
        return records.map { (column, field) ->
            "${column.name}: ${field.value}"
        }.joinToString("; ")
    }

    override fun hashCode(): Int {
        return toString().hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return other?.toString() == toString()
    }
}
