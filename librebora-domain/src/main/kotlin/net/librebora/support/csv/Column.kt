package net.librebora.support.csv

interface Column {
    val index: Int
    val name: String
}
