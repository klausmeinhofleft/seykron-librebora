package net.librebora.support.csv

import net.openhft.hashing.LongHashFunction
import java.io.File
import java.nio.ByteBuffer
import java.util.concurrent.ConcurrentHashMap
import kotlin.math.absoluteValue

typealias Shard = MutableMap<Long, Position>

class Index(private val indexedColumns: Map<String, Int>,
            private val numberOfShards: Int = DEFAULT_NUMBER_OF_SHARDS) {

    companion object {
        private const val ENTRY_SIZE: Int = 24
        private const val VERSION: String = "1"
        private const val DEFAULT_NUMBER_OF_SHARDS: Int = 4
    }

    private val shards: MutableMap<Int, Shard> = ConcurrentHashMap()

    fun put(position: Position,
            row: List<Field>) {

        indexedColumns.forEach { (name, index) ->
            if (row.size > index) {
                val field = row[index]
                val hash = calculateHash(name, field.value)

                resolveShard(hash)[hash] = position
            }
        }
    }

    fun get(
        columnName: String,
        value: String
    ): Position? {
        val hash = calculateHash(columnName, value)
        return resolveShard(hash)[hash]
    }

    fun writeTo(indexDir: String) {
        shards.forEach { (shardId, shard) ->
            val buffer = ByteBuffer.allocateDirect(shard.size * ENTRY_SIZE)
            val output = File(indexDir, "$shardId.$VERSION").outputStream()

            output.use {
                shard.forEach { (key, record) ->
                    buffer.putLong(key)
                    buffer.putLong(record.start)
                    buffer.putLong(record.end)
                }

                buffer.rewind()
                output.channel.write(buffer)
            }
        }
    }

    private fun resolveShard(hash: Long): Shard {
        val shardId: Int = hash.rem(numberOfShards).absoluteValue.toInt()

        return shards.getOrPut(shardId) {
            ConcurrentHashMap()
        }
    }

    private fun calculateHash(columnName: String,
                              value: String): Long {
        return LongHashFunction.xx().hashChars("$columnName:$value")
    }
}
