package net.librebora.support.csv

import java.io.RandomAccessFile

fun<T> session(
    csvFile: String,
    index: Index,
    callback: () -> T
): T {

    try {
        Session.init(
            csvFile = csvFile,
            index = index
        )

        return callback()
    } finally {
        Session.cleanUp()
    }
}

class Session(
    private val csvFile: String,
    private val index: Index
) {

    companion object {
        private val currentSession = ThreadLocal<Session>()

        fun current(): Session =
            currentSession.get()

        fun init(
            csvFile: String,
            index: Index
        ) {
            currentSession.set(Session(
                csvFile = csvFile,
                index = index
            ))
        }

        fun cleanUp() {
            currentSession.remove()
        }
    }

    private val handle = RandomAccessFile(csvFile, "r")

    fun find(vararg query: Pair<String, String>): List<List<Field>> {
        return query.map { (column, value) ->
            index.get(column, value)
        }.map { record ->
            record?.let {
                read(record)
            } ?: emptyList()
        }
    }

    private fun read(record: Position): List<Field> {
        val data = ByteArray((record.end - record.start).toInt())

        handle.seek(record.start)
        handle.read(data)

        return Parser.parseRecord(data)
    }
}
