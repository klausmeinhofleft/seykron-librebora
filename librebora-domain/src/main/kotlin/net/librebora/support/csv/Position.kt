package net.librebora.support.csv

data class Position(
    val start: Long,
    val end: Long
)
