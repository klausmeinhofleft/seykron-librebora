package net.librebora.support.csv

import kotlin.reflect.full.primaryConstructor

open class EntityOperations(val table: Table) {
    inline fun<reified T : Entity> find(vararg query: Pair<Column, String>): List<T> {
        val session = Session.current()
        val resolvedQuery = query.map { (column, value) ->
            column.name to value
        }

        return session.find(*resolvedQuery.toTypedArray()).map { record ->
            val entity: T = T::class.primaryConstructor?.call()
                ?: throw RuntimeException("Cannot instantiate entity with default constructor.")

            entity.records = table.columns.map { column ->
                column to record[column.index]
            }.toMap(mutableMapOf())

            entity
        }
    }
}
